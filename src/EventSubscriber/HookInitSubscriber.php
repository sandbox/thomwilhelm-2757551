<?php

namespace Drupal\hook_init\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class HookInitSubscriber implements EventSubscriberInterface {

  public function runHookInit(GetResponseEvent $event) {

    // Invoke hook_init declarations.
    \Drupal::moduleHandler()->invokeAll('init');

  }

  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('runHookInit');
    return $events;
  }

}